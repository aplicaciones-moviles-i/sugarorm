package facci.pm.sugarorm;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolderBooks> {

    List<Book> listaBooks;

    public BookAdapter(List<Book> listaBooks) {
        this.listaBooks = listaBooks;
    }

    @NonNull
    @Override
    public ViewHolderBooks onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.book_item_list, null, false);
        return new ViewHolderBooks(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderBooks viewHolderBooks, int i) {
        viewHolderBooks.textViewNombre.setText(listaBooks.get(i).getTitle());
        viewHolderBooks.textViewEdicion.setText(listaBooks.get(i).getEdition());
        Picasso.get().load(listaBooks.get(i).getUrlImage()).into(viewHolderBooks.foto);
    }

    @Override
    public int getItemCount() {
        return listaBooks.size();
    }

    public class ViewHolderBooks extends RecyclerView.ViewHolder {

        TextView textViewNombre, textViewEdicion;
        ImageView foto;

        public ViewHolderBooks(View itemView) {
            super(itemView);

            textViewNombre = (TextView) itemView.findViewById(R.id.textViewTitulo);
            textViewEdicion = (TextView) itemView.findViewById(R.id.textViewEdicion);
            foto = (ImageView) itemView.findViewById(R.id.imageViewLista);
        }
    }

}
