package facci.pm.sugarorm;

import com.orm.SugarRecord;

public class Book extends SugarRecord<Book> {

    //Propiedades
    String title;
    String edition;
    String urlImage;
    Author author;

    //Constructor
    public Book() {
    }

    //Sobrecarga del constructor
    public Book(String title, String edition, String urlImage) {
        this.title = title;
        this.edition = edition;
        this.urlImage = urlImage;
    }

    //Encapsulamiento de los campos
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
