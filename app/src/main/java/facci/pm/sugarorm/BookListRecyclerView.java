package facci.pm.sugarorm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public class BookListRecyclerView extends AppCompatActivity {

    RecyclerView recyclerViewBooks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_list_recycler_view);

        List<Book> books = Book.listAll(Book.class);
        recyclerViewBooks = (RecyclerView)findViewById(R.id.recyclerViewBooks);
        recyclerViewBooks.setLayoutManager(new LinearLayoutManager(this));


        BookAdapter bookAdapter = new BookAdapter(books);
        recyclerViewBooks.setAdapter(bookAdapter);


    }
}