package facci.pm.sugarorm;

import com.orm.SugarRecord;

public class Author extends SugarRecord<Author> {

    String name;

    public Author(String name) {
        this.name = name;
    }

    public Author() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
